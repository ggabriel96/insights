"""initial migration

Revision ID: 93425d43e54f
Revises:
Create Date: 2022-03-08 15:29:20.725720

"""
import sqlalchemy as sa
import sqlalchemy_utc
from alembic import op

# revision identifiers, used by Alembic.
revision = "93425d43e54f"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "card",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=False),
        sa.Column("modified_at", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=False),
        sa.Column("text", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk-card")),
    )
    op.create_index(op.f("ix-card-created_at"), "card", ["created_at"], unique=False)
    op.create_index(op.f("ix-card-modified_at"), "card", ["modified_at"], unique=False)
    op.create_table(
        "tag",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("card_id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=64), nullable=False),
        sa.ForeignKeyConstraint(["card_id"], ["card.id"], name=op.f("fk-tag-card_id-card")),
        sa.PrimaryKeyConstraint("id", name=op.f("pk-tag")),
    )
    op.create_index(op.f("ix-tag-card_id_name"), "tag", ["card_id", "name"], unique=True)


def downgrade():
    op.drop_index(op.f("ix-tag-card_id_name"), table_name="tag")
    op.drop_table("tag")
    op.drop_index(op.f("ix-card-modified_at"), table_name="card")
    op.drop_index(op.f("ix-card-created_at"), table_name="card")
    op.drop_table("card")
