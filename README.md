# Insights app

## Configuração rápida

1. Este projeto utiliza o [Poetry](https://python-poetry.org/) para gerenciar suas dependências.
    Consulte a [documentação oficial](https://python-poetry.org/docs/#installation) para instalá-lo se ainda não o fez.
    **Este passo só precisa ser executado uma vez.**

1. Configure um ambiente virtual. **A versão utilizada do Python é a 3.10**.

    - Se você não deseja que o Poetry gerencie seus ambientes virtuais, configure-o para não criar novos ambientes virtuais automaticamente
        ([docs](https://python-poetry.org/docs/faq/#i-dont-want-poetry-to-manage-my-virtual-environments-can-i-disable-it)).
        **Este passo só precisa ser executado uma vez.**

        ```sh
        poetry config virtualenvs.create false
        ```

    - Se você usa o [pyenv](https://github.com/pyenv/pyenv),
        os dois [funcionam em conjunto](https://python-poetry.org/docs/managing-environments/).

    - Se você usa o [conda](https://docs.conda.io/en/latest/miniconda.html), siga também o passo-a-passo colapsado abaixo:

        <details>
        <summary>Configuração do conda</summary>

        1. Crie um novo ambiente virtual:

            ```sh
            conda env create -f environment.yml
            ```

        1. Ative o ambiente virtual:

            ```sh
            conda activate insights
            ```

        </details>

1. Instale as dependências do backend:

    ```sh
    poetry install
    ```

1. Configure o pre-commit:

    ```sh
    pre-commit install
    ```

1. **Se precisar** sobrescrever as configurações definidas no [`insights/settings.py`](insights/settings.py), crie um arquivo `.env` baseado no `template.env`.
    Também já existe o arquivo [`tests.env`](tests.env) que é usado na execução dos testes.
    Com este arquivo é possível modificar a URI de conexão ao banco sem alterar código commitado ou definir variáveis de ambiente no sistema operacional.

    ```sh
    cp template.env .env
    ```

1. Se quiser utilizar um banco MySQL ao invés do padrão SQLite, siga também o passo-a-passo colapsado abaixo.
    Configurar o PostgreSQL ao invés do MySQL é análogo.

    <details>
    <summary>Configuração do MySQL</summary>

    Este passo-a-passo utiliza o [Docker](https://www.docker.com/).
    Para instalá-lo, siga as [instruções oficiais](https://docs.docker.com/engine/install/) ou a forma recomendada do seu sistema operacional.

    1. Escolha e instale [uma dependência necessária](https://docs.sqlalchemy.org/en/14/dialects/mysql.html#dialect-mysql) para se conectar ao MySQL.
        Exemplo:

        ```
        poetry add PyMySQL
        ```

    1. Crie uma instância do MySQL (**este passo só precisa ser executado uma vez**):

        ```sh
        docker create \
            --env MYSQL_ROOT_PASSWORD=1234 \
            --name mysql \
            -p 3306:3306 \
            mysql:8.0 \
            --default-authentication-plugin=mysql_native_password
        ```

    1. Uma vez criado o container para o MySQL, basta iniciá-lo:

        ```sh
        docker start mysql
        ```

    1. Por fim, resta apenas criar os bancos de dados usados pelo projeto.
        Os nomes dos bancos devem corresponder aos utilizados nas URIs do `.env` customizado criado no passo anterior.
        Exemplo:

        ```sh
        mysql -h 127.0.0.1 -u root -p1234 -e "CREATE DATABASE insights; CREATE DATABASE insights_tests";
        ```

1. Execute as _migrations_ para gerar as tabelas:

    ```sh
    poetry run init-db
    ```

1. Instale as dependências do frontend:

    ```sh
    cd web-app/ && npm ci
    ```

1. Gere o client TypeScript.
    Repita este passo sempre que models ou controllers forem alterados no backend.
    Caso tenha problemas para executar este passo, consulte [a seção abaixo](#geração-do-API-client).

    ```sh
    # cd ..  # está na pasta web-app/ se fez passo acima
    poetry run echo-openapi > openapi.json
    cd web-app/ && npm run client-ts
    ```

1. Se você usa o PyCharm, siga também o passo-a-passo colapsado abaixo:

    <details>
    <summary>Configuração do PyCharm</summary>

    #### Interpretador (para ambientes `conda`)

    1. Abra as configurações PyCharm
    1. Navegue até a configuração do interpretador, dentro das configurações do projeto
    1. Na engrenagem, selecione para adicionar um interpretador
    1. No novo diálogo que abrir, selecione `Conda Environment`, então `Existing environment`
    1. Selecione o ambiente virtual criado para o projeto

    #### Node.js (para ambientes `conda`)

    1. Abra as configurações PyCharm
    1. Expanda a seção `Languages & Frameworks`
    1. Selecione o item `Node.js`
    1. No menu `Node interpreter`, encontre e selecione o binário `node` do seu ambiente virtual,
        e.g. `~/miniconda3/envs/insights/bin/node`

    #### Testes

    1. Abra as configurações PyCharm
    1. Expanda a seção `Tools`
    1. Selecione o item `Python Integrated Tools`
    1. Em `Default test runner`, escolha o `Autodetect` (já deve aparecer Pytest entre parênteses)
    1. Confirme e feche

    </details>

1. Rode a app localmente.
    O PyCharm está configurado para rodar o back e o front (e os testes).
    De qualquer forma, para rodar manualmente pelo terminal, execute:

    ```sh
    uvicorn insights.server:app --reload --host 0.0.0.0 --port 8080  # backend
    cd web-app/ && npm run start -- --open  # frontend
    ```

1. Rode os testes do backend.
    O PyCharm está configurado para rodar os testes.
    De qualquer forma, para rodar manualmente pelo terminal, execute:

    ```sh
    pytest \
        --cov=insights/ \
        --cov-report=html \
        --junit-xml=test-results/pytest/results.xml \
        --verbose \
        insights/tests/
    ```

### Acessando a aplicação web e o CLI

**Após seguir a configuração rápida acima**:

1. a app web estará disponível em http://localhost:4200.
1. o backend estará respondendo em http://localhost:8080.
1. o Swagger do backend estará disponível em http://localhost:8080/api/docs.
1. o Redoc do backend estará disponível em http://localhost:8080/api/redoc.
1. o JSON da especificação OpenAPI do backend estará disponível em http://localhost:8080/api/openapi.json.
1. também existe uma app de linha de comando que pode ser usada para importar insights.
    Consulte a ajuda com o comando abaixo:

    ```shell
    python insights/cli.py --help
    ```

### Geração do API client

Este projeto utiliza o [OpenAPI Generator](https://openapi-generator.tech/) para gerar clientes para o backend automaticamente.
Como ele depende do Java >= 8 para rodar, certifique-se que ele está instalado.
Uma forma fácil de fazer isso é usando o próprio ambiente conda do projeto.
Exemplo:

```sh
conda install -c conda-forge openjdk=8
```

## Observações sobre a solução

Com o prazo limite para entregar a solução, fiz algumas escolhas de implementação que acredito que valem ser comentadas aqui.

- só escrevi _alguns_ testes no backend para ilustrar como eles são escritos, organizados, e executados.
- gostaria de ter construído um layout responsivo que se adaptasse a telas de diferentes tamanhos.
    Entretanto, optei por construir apenas a interface móvel de referência "simulando" uma tela limitada de um celular.
    A interface também não ficou **exatamente** como nas especificações.
- apesar de eu acreditar que i18n é indispensável, não o fiz.
- não há CI configurado.
