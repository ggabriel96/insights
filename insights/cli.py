import csv
from pathlib import Path

import typer

from insights import database
from insights.models.card import CardUpsert
from insights.repository.card import CardRepository
from insights.service.card import CardService
from insights.settings import Environment


def main(file: Path = typer.Argument(..., help="path to the CSV file to import")) -> None:
    """Import cards from a CSV file into the Insights app"""
    if not file.exists():
        typer.secho(f"Error: file `{file}` does not exist", fg=typer.colors.RED, err=True)
        raise typer.Exit(code=1)
    if file.is_dir():
        typer.secho(f"Error: file `{file}` is a directory", fg=typer.colors.RED, err=True)
        raise typer.Exit(code=2)

    env = Environment()
    database.init(env)
    with database.SESSION_MAKER.begin() as session:
        card_service = CardService(CardRepository(session))
        with file.open("r") as csv_file:
            reader = csv.reader(csv_file)
            next(reader, None)  # skip header
            card_service.create_cards(
                CardUpsert(text=line[0], tags=line[1].strip().split(";") if line[1] else None) for line in reader
            )


if __name__ == "__main__":
    typer.run(main)
