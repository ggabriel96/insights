import json
import sys

from alembic import command
from alembic.config import Config

from insights.server import app


def echo_openapi_spec() -> None:
    json.dump(app.openapi(), sys.stdout)


def init_database() -> None:
    alembic_cfg = Config("alembic.ini")
    command.upgrade(alembic_cfg, "head")
