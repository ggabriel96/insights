from fastapi import Depends
from sqlalchemy.orm import Session

from insights.database import get_session


class BaseRepository:
    def __init__(self, session: Session = Depends(get_session)) -> None:
        self._session = session

    def flush(self) -> None:
        self._session.flush()
