import datetime as dt
from typing import Optional

from sqlalchemy import asc, delete, desc, select
from sqlalchemy.orm import joinedload

from insights.database.models import Card as CardDB
from insights.database.models import Tag as TagDB
from insights.models.card import CardSearch, CardUpsert
from insights.models.search import Ordering
from insights.repository import BaseRepository


class CardRepository(BaseRepository):
    def delete_card(self, card_id: int) -> None:
        query = delete(CardDB).where(CardDB.id == card_id)
        self._session.execute(query)

    def delete_tag(self, card_id: int, tag_name: str) -> None:
        query = delete(TagDB).where(TagDB.card_id == card_id, TagDB.name == tag_name)
        self._session.execute(query)

    def delete_tags(self, card_id: int) -> None:
        query = delete(TagDB).where(TagDB.card_id == card_id)
        self._session.execute(query)

    def get_card_by_id(self, card_id: int, *, include_tags: bool) -> Optional[CardDB]:
        query = select(CardDB).where(CardDB.id == card_id)
        if include_tags:
            query = query.options(joinedload(CardDB.tag_objects))
        return self._session.execute(query).unique().scalar_one_or_none()

    def insert_card(self, card_upsert: CardUpsert, created_at: Optional[dt.datetime] = None) -> CardDB:
        """Insert a new card to the database.

        :param card_upsert: the data of the card to insert.
        :param created_at: the date of creation of the card. Defaults to now if not provided.
                Useful to provide when creating multiple cards so that all of them end up as created at the same time.
        :return: the created card.
        """
        created_at = created_at or dt.datetime.now(tz=dt.timezone.utc)
        card = CardDB(created_at=created_at, modified_at=created_at, text=card_upsert.text)
        if card_upsert.tags:
            card.tags.extend(card_upsert.tags)
        self._session.add(card)
        return card

    def search_cards(self, card_search: CardSearch) -> list[CardDB]:
        """Search existing cards, optionally filtering by their tags.

        :param card_search: the search criteria.
            It is possible to specify the sort order of the results and how to paginate them via the options attribute.
        :return: the list of cards that satisfy the provided criteria.
        """
        direction = desc if card_search.options.ordering == Ordering.DESCENDING else asc
        query = select(CardDB).order_by(direction(CardDB.id)).options(joinedload(CardDB.tag_objects))
        if card_search.tags:
            query = query.where(CardDB.tags.in_(card_search.tags))
        if pagination := card_search.options.pagination:
            query = query.limit(pagination.page_size)
            query = query.offset((pagination.page_number - 1) * pagination.page_size)
        return self._session.execute(query).unique().scalars().all()
