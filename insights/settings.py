from functools import lru_cache

from pydantic import BaseSettings


class Environment(BaseSettings):
    class Config:
        env_file = ".env"

    db_uri: str = "sqlite+pysqlite:///insights.sqlite3"
    environment: str = "LOCAL"


@lru_cache
def get_environment() -> Environment:
    return Environment()
