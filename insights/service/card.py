import datetime as dt
from typing import Iterator, Optional

from fastapi import Depends

from insights.database.models import Card as CardDB
from insights.models.card import CardSearch, CardUpsert
from insights.repository.card import CardRepository


class CardService:
    def __init__(self, card_repository: CardRepository = Depends()) -> None:
        self._card_repository = card_repository

    def add_tag_to_card(self, card_id: int, tag_name: str) -> Optional[CardDB]:
        """Add a tag to a card (only if it does not yet have it).

        :param card_id: the ID of the card to add the tag to.
        :param tag_name: the tag to add to the card.
        :return: the updated card.
        """
        if card := self.get_card(card_id, include_tags=True):
            if tag_name not in card.tags:
                card.tags.append(tag_name)
        return card

    def create_card(self, card_upsert: CardUpsert) -> CardDB:
        """Create a card.

        Immediately performs a flush in order to get the database-generated ID.

        :param card_upsert: the data of the card to create.
        :return: the created card.
        """
        card = self._card_repository.insert_card(card_upsert)
        self._card_repository.flush()
        return card

    def create_cards(self, cards_upsert: Iterator[CardUpsert]) -> None:
        """Create multiple cards.

        Only performs a flush at the end of all insertions, so this one is preferable over multiple calls to `insert`.

        :param cards_upsert: the data of all cards to insert.
        :return: None.
        """
        for card in cards_upsert:
            self._card_repository.insert_card(card)
        self._card_repository.flush()

    def delete_card(self, card_id: int) -> None:
        self._card_repository.delete_card(card_id)

    def get_card(self, card_id: int, *, include_tags: bool) -> Optional[CardDB]:
        return self._card_repository.get_card_by_id(card_id, include_tags=include_tags)

    def remove_tag_from_card(self, card_id: int, tag_name: str) -> Optional[CardDB]:
        self._card_repository.delete_tag(card_id, tag_name)
        return self.get_card(card_id, include_tags=True)

    def search_cards(self, card_search: CardSearch) -> list[CardDB]:
        return self._card_repository.search_cards(card_search)

    def update_card(self, card_id: int, card_upsert: CardUpsert) -> Optional[CardDB]:
        """Update a card, completely replacing its contents.

        :param card_id: the ID of the card to update.
        :param card_upsert: the data of the card to update.
        :return: the updated card.
        """
        if card := self.get_card(card_id, include_tags=True):
            card.text = card_upsert.text
            card.modified_at = dt.datetime.now(tz=dt.timezone.utc)
            if card_upsert.tags is not None:
                self._card_repository.delete_tags(card_id)
                card.tags = card_upsert.tags
        return card

    def update_tag(self, *, card_id: int, old_name: str, new_name: str) -> Optional[CardDB]:
        if card := self.get_card(card_id, include_tags=True):
            try:
                tag_idx = card.tags.index(old_name)
            except ValueError:
                pass
            else:
                card.tag_objects[tag_idx].name = new_name
        return card
