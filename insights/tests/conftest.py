from typing import Iterator

from pytest import fixture
from sqlalchemy import MetaData
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, close_all_sessions, sessionmaker

from insights import database
from insights.database.models import metadata
from insights.settings import Environment


@fixture(name="env", scope="session")
def _env() -> Environment:
    return Environment(_env_file="tests.env")  # type: ignore[call-arg]


@fixture(name="init_db", scope="session")
def _init_db(env: Environment) -> None:
    database.init(env)
    metadata.create_all(database.ENGINE)
    database.SESSION_MAKER.configure(expire_on_commit=False)


@fixture(name="session_maker", scope="function")
def _session_maker(init_db: None) -> Iterator[sessionmaker]:  # type: ignore[type-arg] # pylint: disable=unused-argument
    """
    This fixture provides a `SessionMaker`, allowing for the control of transactions, with auto-truncate on teardown.
    Use the `db_session` fixture for tests that don't span across transactions.
    """
    yield database.SESSION_MAKER
    close_all_sessions()
    truncate(database.ENGINE)


@fixture(name="db_session", scope="function")
def _db_session(init_db: None) -> Iterator[Session]:  # pylint: disable=unused-argument
    """
    This fixture provides a SQLAlchemy `Session`, which is effectively a database transaction,
    with auto-rollback on teardown.
    Hence, it will generally be used in tests that don't span across transactions.
    Use the `session_maker` fixture for tests that need control over the transactions.
    """
    with database.SESSION_MAKER.begin() as session:
        yield session
        session.rollback()


def truncate(engine: Engine) -> None:
    meta = MetaData()
    meta.reflect(bind=engine)
    with engine.begin() as conn:
        for table in reversed(meta.sorted_tables):
            conn.execute(table.delete())
