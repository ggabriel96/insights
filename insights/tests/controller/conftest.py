from fastapi.testclient import TestClient
from pytest import fixture
from sqlalchemy.orm import sessionmaker

from insights.server import app
from insights.settings import Environment, get_environment


@fixture(name="client", scope="function")
def _client(
    env: Environment,
) -> TestClient:
    app.dependency_overrides[get_environment] = lambda: env
    return TestClient(app, raise_server_exceptions=True)


@fixture(name="session_maker", scope="function", autouse=True)
def _session_maker(session_maker: sessionmaker) -> sessionmaker:  # type: ignore[type-arg]
    """
    Just overrides the "global" session_maker fixture so that controller tests
    automatically use them, guaranteeing that the database is truncated.
    """
    return session_maker
