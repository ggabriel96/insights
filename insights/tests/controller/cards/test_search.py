import datetime as dt
from typing import Optional

from fastapi import status
from fastapi.testclient import TestClient
from pydantic import parse_raw_as
from pytest import mark
from sqlalchemy.orm import sessionmaker

from insights.database.models import Card as CardDB
from insights.models.card import Card, CardSearch
from insights.models.search import Ordering, Pagination, SearchOptions

URL = "/api/cards/search"


def test_no_cards_exist_should_return_empty_list(client: TestClient) -> None:
    # Arrange
    card_search = CardSearch()

    # Act
    response = client.post(URL, data=card_search.json())

    # Assert
    assert response.status_code == status.HTTP_200_OK


@mark.parametrize("tags", [None, []])
def test_tags_is_falsy_should_return_everything(
    tags: Optional[list[str]], client: TestClient, session_maker: sessionmaker  # type: ignore[type-arg]
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    cards = [
        CardDB(text="1", tags=["1"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="2", tags=["2"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="3", tags=["3"], created_at=now, modified_at=now),  # type: ignore[call-arg]
    ]
    with session_maker.begin() as session:
        session.add_all(cards)
    card_search = CardSearch(tags=tags)

    # Act
    response = client.post(URL, data=card_search.json())

    # Assert
    assert response.status_code == status.HTTP_200_OK
    response_cards = parse_raw_as(list[Card], response.text)
    assert len(response_cards) == 3
    assert response_cards[0].id == cards[2].id
    assert response_cards[1].id == cards[1].id
    assert response_cards[2].id == cards[0].id


def test_tags_is_nonempty_list_should_return_cards_with_any_of_those_tags(
    client: TestClient, session_maker: sessionmaker  # type: ignore[type-arg]
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    cards = [
        CardDB(text="1", tags=["0", "1", "11", "111"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="2", tags=["2", "22", "222"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="3", tags=["3", "0"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="4", tags=["4"], created_at=now, modified_at=now),  # type: ignore[call-arg]
    ]
    with session_maker.begin() as session:
        session.add_all(cards)
    card_search = CardSearch(tags=["0", "4"])

    # Act
    response = client.post(URL, data=card_search.json())

    # Assert
    assert response.status_code == status.HTTP_200_OK
    response_cards = parse_raw_as(list[Card], response.text)
    assert len(response_cards) == 3
    assert response_cards[0].id == cards[3].id
    assert response_cards[1].id == cards[2].id
    assert response_cards[2].id == cards[0].id


def test_no_tags_on_second_page_and_ascending_order(
    client: TestClient, session_maker: sessionmaker  # type: ignore[type-arg]
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    cards = [
        CardDB(text="6", tags=["1"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="5", tags=["2"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="4", tags=["3"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="3", tags=["4"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="2", tags=["5"], created_at=now, modified_at=now),  # type: ignore[call-arg]
        CardDB(text="1", tags=["6"], created_at=now, modified_at=now),  # type: ignore[call-arg]
    ]
    with session_maker.begin() as session:
        session.add_all(cards)
    card_search = CardSearch(
        tags=None, options=SearchOptions(ordering=Ordering.ASCENDING, pagination=Pagination(page_number=2, page_size=2))
    )

    # Act
    response = client.post(URL, data=card_search.json())

    # Assert
    assert response.status_code == status.HTTP_200_OK
    response_cards = parse_raw_as(list[Card], response.text)
    assert len(response_cards) == 2
    assert response_cards[0].id == cards[2].id
    assert response_cards[1].id == cards[3].id
