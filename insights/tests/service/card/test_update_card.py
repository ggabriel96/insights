import datetime as dt

from sqlalchemy import select
from sqlalchemy.orm import Session, joinedload

from insights.database.models import Card as CardDB
from insights.models.card import CardUpsert
from insights.service.card import CardService


def test_card_does_not_exist_should_return_none(db_session: Session, card_service: CardService) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card = CardDB(text="text", created_at=now, modified_at=now, tags=["A", "B"])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()
    card_id = card.id + 1
    card_upsert = CardUpsert(text="nonexistent", tags=None)

    # Act
    card_updated = card_service.update_card(card_id, card_upsert)

    # Assert
    assert card_updated is None

    # Assert we didn't do anything in the database
    cards_db = db_session.execute(select(CardDB).options(joinedload(CardDB.tag_objects))).unique().scalars().all()
    assert len(cards_db) == 1
    assert cards_db[0].text == card.text
    assert cards_db[0].tags == card.tags
    assert cards_db[0].created_at == card.created_at
    assert cards_db[0].modified_at == card.modified_at
    assert cards_db[0].created_at == cards_db[0].modified_at


def test_card_without_tags_and_no_tags_to_update_should_only_update_text(
    db_session: Session, card_service: CardService
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card_upsert = CardUpsert(text="text2", tags=None)
    card = CardDB(text="text1", created_at=now, modified_at=now, tags=[])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()

    # Act
    card_updated = card_service.update_card(card.id, card_upsert)

    # Assert
    assert card_updated is not None
    assert card_updated.text == card_upsert.text
    assert card_updated.tags == card.tags
    assert card_updated.created_at == now
    assert card_updated.modified_at > now


def test_card_without_tags_and_tags_to_update_should_update_text_and_tags(
    db_session: Session, card_service: CardService
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card_upsert = CardUpsert(text="text2", tags=["C", "D"])
    card = CardDB(text="text1", created_at=now, modified_at=now, tags=[])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()

    # Act
    card_updated = card_service.update_card(card.id, card_upsert)

    # Assert
    assert card_updated is not None
    assert card_updated.text == card_upsert.text
    assert card_updated.tags == card_upsert.tags
    assert card_updated.created_at == now
    assert card_updated.modified_at > now


def test_card_with_tags_and_no_tags_to_update_should_only_update_text(
    db_session: Session, card_service: CardService
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card_upsert = CardUpsert(text="text2", tags=None)
    card = CardDB(text="text1", created_at=now, modified_at=now, tags=["A", "B"])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()

    # Act
    card_updated = card_service.update_card(card.id, card_upsert)

    # Assert
    assert card_updated is not None
    assert card_updated.text == card_upsert.text
    assert card_updated.tags == card.tags
    assert card_updated.created_at == now
    assert card_updated.modified_at > now


def test_card_with_tags_and_tags_to_update_should_update_text_and_tags(
    db_session: Session, card_service: CardService
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card_upsert = CardUpsert(text="text2", tags=["C", "D"])
    card = CardDB(text="text1", created_at=now, modified_at=now, tags=["A", "B"])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()

    # Act
    card_updated = card_service.update_card(card.id, card_upsert)

    # Assert
    assert card_updated is not None
    assert card_updated.text == card_upsert.text
    assert card_updated.tags == card_upsert.tags
    assert card_updated.created_at == now
    assert card_updated.modified_at > now


def test_card_with_tags_and_tags_to_update_is_empty_list_should_update_text_and_clear_tags(
    db_session: Session, card_service: CardService
) -> None:
    # Arrange
    now = dt.datetime.now(tz=dt.timezone.utc)
    card_upsert = CardUpsert(text="text2", tags=[])
    card = CardDB(text="text1", created_at=now, modified_at=now, tags=["A", "B"])  # type: ignore[call-arg]
    db_session.add(card)
    db_session.flush()
    db_session.expunge_all()

    # Act
    card_updated = card_service.update_card(card.id, card_upsert)

    # Assert
    assert card_updated is not None
    assert card_updated.text == card_upsert.text
    assert card_updated.tags == card_upsert.tags == []
    assert card_updated.created_at == now
    assert card_updated.modified_at > now
