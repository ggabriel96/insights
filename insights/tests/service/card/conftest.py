from pytest import fixture

from insights.repository.card import CardRepository
from insights.service.card import CardService


@fixture(name="card_service", scope="function")
def _card_service(card_repository: CardRepository) -> CardService:
    return CardService(card_repository)
