from pytest import fixture
from sqlalchemy.orm import Session

from insights.repository.card import CardRepository


@fixture(name="db_session", scope="function", autouse=True)
def _db_session(db_session: Session) -> Session:
    """
    Just overrides the "global" db_session fixture so that service tests
    automatically use them, guaranteeing that the transaction is rolled back.
    """
    return db_session


@fixture(name="card_repository", scope="function")
def _card_repository(db_session: Session) -> CardRepository:
    return CardRepository(db_session)
