from fastapi import APIRouter

from insights.controller import cards

router = APIRouter(prefix="/api")
router.include_router(cards.router)
