# pylint: disable=unused-argument
from fastapi import APIRouter, Depends, HTTPException, Path, Query, Response, status

from insights.database.models import Card as CardDB
from insights.models.card import Card, CardSearch, CardUpsert
from insights.models.error import HTTPError
from insights.models.tag import TAG_NAME_MAX_LENGTH, TAG_NAME_MIN_LENGTH, TagUpdate
from insights.service.card import CardService

router = APIRouter(prefix="/cards", tags=["cards"])


@router.post(
    "",
    summary="Create a card",
    response_model=Card,
    responses={},
)
def create(card_upsert: CardUpsert, card_service: CardService = Depends()) -> CardDB:
    return card_service.create_card(card_upsert)


@router.delete(
    "/{card_id}",
    summary="Delete a card",
    response_class=Response,
    response_model=None,
    responses={},
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete(card_id: int, card_service: CardService = Depends()) -> None:
    card_service.delete_card(card_id)


@router.get(
    "/{card_id}",
    summary="Get card by ID",
    response_model=Card,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "description": "No card found for provided ID",
            "model": HTTPError,
        }
    },
)
def get(
    card_id: int,
    include_tags: bool = Query(True, description="Whether or not to load the tags of the card"),
    card_service: CardService = Depends(),
) -> CardDB:
    if card := card_service.get_card(card_id, include_tags=include_tags):
        return card
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@router.post(
    "/search",
    summary="Search existing cards",
    description="Search existing cards, optionally filtering by their tags.",
    response_model=list[Card],
    responses={},
)
def search(card_search: CardSearch, card_service: CardService = Depends()) -> list[CardDB]:
    return card_service.search_cards(card_search)


@router.patch(
    "/{card_id}/tags/{tag_name}",
    summary="Add a tag to a card",
    description="Add a tag to a card (only if it does not yet have it).",
    response_model=Card,
    response_description="The card identified by the provided ID with its associated tags (including the new one)",
    responses={
        status.HTTP_404_NOT_FOUND: {
            "description": "No card found for provided ID",
            "model": HTTPError,
        }
    },
)
def tag_add(
    card_id: int,
    tag_name: str = Path(..., max_length=TAG_NAME_MAX_LENGTH, min_length=TAG_NAME_MIN_LENGTH),
    card_service: CardService = Depends(),
) -> CardDB:
    if card := card_service.add_tag_to_card(card_id, tag_name):
        return card
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@router.delete(
    "/{card_id}/tags/{tag_name}",
    summary="Remove a tag from a card",
    response_model=Card,
    response_description="The card identified by the provided ID with its associated tags (excluding the removed one)",
    responses={
        status.HTTP_404_NOT_FOUND: {
            "description": "No card found for provided ID",
            "model": HTTPError,
        }
    },
)
def tag_remove(
    card_id: int,
    tag_name: str = Path(..., max_length=TAG_NAME_MAX_LENGTH, min_length=TAG_NAME_MIN_LENGTH),
    card_service: CardService = Depends(),
) -> CardDB:
    if card := card_service.remove_tag_from_card(card_id, tag_name):
        return card
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@router.put(
    "/{card_id}/tags/{tag_name}",
    summary="Update a tag of a card",
    response_model=Card,
    response_description="The card identified by the provided ID with its associated tags (including the updated one)",
    responses={
        status.HTTP_404_NOT_FOUND: {
            "description": "No card found for provided ID",
            "model": HTTPError,
        }
    },
)
def tag_update(
    card_id: int,
    tag_update: TagUpdate,  # pylint: disable=redefined-outer-name
    tag_name: str = Path(..., max_length=TAG_NAME_MAX_LENGTH, min_length=TAG_NAME_MIN_LENGTH),
    card_service: CardService = Depends(),
) -> CardDB:
    if card := card_service.update_tag(card_id=card_id, old_name=tag_name, new_name=tag_update.name):
        return card
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@router.put(
    "/{card_id}",
    summary="Update a card",
    description="Update a card, completely replacing its contents.",
    response_model=Card,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "description": "No card found for provided ID",
            "model": HTTPError,
        }
    },
)
def update(card_id: int, card_upsert: CardUpsert, card_service: CardService = Depends()) -> CardDB:
    if card := card_service.update_card(card_id, card_upsert):
        return card
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
