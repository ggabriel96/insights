import datetime as dt

from sqlalchemy import Column, ForeignKey, Index, Integer, MetaData, String, Text
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy_utc import UtcDateTime

from insights.models.tag import TAG_NAME_MAX_LENGTH

convention = {
    "ck": "ck-%(table_name)s-%(constraint_name)s",
    "fk": "fk-%(table_name)s-%(column_0_N_name)s-%(referred_table_name)s",
    "ix": "ix-%(table_name)s-%(column_0_N_name)s",
    "pk": "pk-%(table_name)s",
    "uq": "uq-%(table_name)s-%(column_0_N_name)s",
}

metadata = MetaData(naming_convention=convention)
Base = declarative_base(metadata=metadata)


class Card(Base):
    __tablename__ = "card"
    id: int = Column(Integer, primary_key=True)

    created_at: dt.datetime = Column(UtcDateTime, index=True, nullable=False, unique=False)
    modified_at: dt.datetime = Column(UtcDateTime, index=True, nullable=False, unique=False)
    text = Column(Text, index=False, nullable=False, unique=False)

    # virtual fields
    tag_objects: list["Tag"] = relationship("Tag", lazy="noload", uselist=True, back_populates="card")
    tags = association_proxy("tag_objects", "name")


class Tag(Base):
    __tablename__ = "tag"
    id: int = Column(Integer, primary_key=True)

    card_id: int = Column(ForeignKey(Card.id), index=False, nullable=False, unique=False)
    name = Column(String(TAG_NAME_MAX_LENGTH), index=False, nullable=False, unique=False)

    # virtual fields
    card = relationship(Card, lazy="noload", uselist=False, back_populates="tag_objects")

    def __init__(self, name: str) -> None:
        self.name = name


Index(
    # index name is None so it gets an automatic one from the naming convention above
    None,  # type: ignore[arg-type]
    Tag.card_id,
    Tag.name,
    unique=True,
)
