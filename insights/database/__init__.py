from typing import Iterator

from fastapi import Request
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, sessionmaker

from insights.settings import Environment

ENGINE: Engine
SESSION_MAKER: sessionmaker  # type: ignore[type-arg]


def init(env: Environment) -> None:
    global ENGINE, SESSION_MAKER  # pylint: disable=global-statement
    ENGINE = create_engine(env.db_uri, echo=True, future=True)
    SESSION_MAKER = sessionmaker(ENGINE)


def get_session(request: Request) -> Iterator[Session]:
    with SESSION_MAKER.begin() as session:
        request.state.db_session = session
        yield session
