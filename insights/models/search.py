from enum import Enum
from typing import Optional

from pydantic import Field, PositiveInt

from insights.models import CRUDModel


class Ordering(str, Enum):
    ASCENDING = "asc"
    DESCENDING = "desc"


class Pagination(CRUDModel):
    page_number: PositiveInt
    page_size: PositiveInt


class SearchOptions(CRUDModel):
    ordering: Ordering = Field(
        Ordering.DESCENDING, description="Whether to sort the results in ascending or descending order of ID"
    )
    pagination: Optional[Pagination] = Field(
        None,
        description="Define how to paginate the results. Sending null returns everything",
    )
