import datetime as dt
from typing import Optional

from pydantic import Field, validator

from insights.models import CRUDModel, OrmModel, listify_association_list
from insights.models.search import SearchOptions
from insights.models.tag import TAG_NAME_MAX_LENGTH, TAG_NAME_MIN_LENGTH


class Card(OrmModel):
    id: int
    created_at: dt.datetime
    modified_at: dt.datetime
    text: str
    tags: list[str]

    # validators
    _listify_tags = validator("tags", allow_reuse=True, pre=True)(listify_association_list)


class CardSearch(CRUDModel):
    options: SearchOptions = SearchOptions()
    tags: Optional[list[str]] = Field(
        None,
        description="Filter the results so that only cards that have any of these tags are returned",
        max_length=TAG_NAME_MAX_LENGTH,
        min_length=TAG_NAME_MIN_LENGTH,
    )

    @validator("tags", each_item=True, pre=True)
    @staticmethod
    def _make_tags_uppercase(tag: str) -> str:
        return tag.upper()


class CardUpsert(CRUDModel):
    text: str = Field(description="The textual content of the card", min_length=1)
    tags: Optional[list[str]] = Field(
        None,
        description=(
            "The list of tags of the card. "
            "In an update, leave null to not modify the existing tags and "
            "send an empty list to remove all tags from the card."
        ),
        max_length=TAG_NAME_MAX_LENGTH,
        min_length=TAG_NAME_MIN_LENGTH,
    )

    @validator("tags", each_item=True, pre=True)
    @staticmethod
    def _make_tags_uppercase(tag: str) -> str:
        return tag.upper()
