from pydantic import Field

from insights.models import CRUDModel


class HTTPError(CRUDModel):
    detail: str = Field(description="A human-friendly message of the error")
