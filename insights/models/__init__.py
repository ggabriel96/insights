from typing import Any, Type

from pydantic import BaseModel, Extra, root_validator
from sqlalchemy.ext.associationproxy import _AssociationList


class NonEmptyModel(BaseModel):
    @classmethod
    @root_validator(pre=True)
    def at_least_one_field_is_set(
        cls: Type[BaseModel],
        values: dict[str, Any],
    ) -> dict[str, Any]:
        if not values:
            raise ValueError(
                f"{cls.__name__} cannot be empty (at least one field must be set)"  # pylint: disable=no-member
            )
        return values


class CRUDModel(NonEmptyModel):
    class Config:
        # we generally don't want CRUD input to be empty, mutable, or have unexpected fields
        allow_mutation = False
        extra = Extra.forbid


class OrmModel(BaseModel):
    class Config:
        orm_mode = True


def listify_association_list(value: Any) -> Any:
    """
    List of objects from SQLAlchemy are actually of type _AssociationList, which Pydantic doesn't recognize.
    So we need this as a pre-validator of such fields so that they get converted to regular lists before validation.
    """
    if isinstance(value, _AssociationList):
        return list(value)
    return value
