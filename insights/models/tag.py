from pydantic import Field, validator

from insights.models import CRUDModel

TAG_NAME_MAX_LENGTH = 64
TAG_NAME_MIN_LENGTH = 1


class TagUpdate(CRUDModel):
    name: str = Field(max_length=TAG_NAME_MAX_LENGTH, min_length=TAG_NAME_MIN_LENGTH)

    @staticmethod
    @validator("name", pre=True)
    def _make_name_uppercase(name: str) -> str:
        return name.upper()
