from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from insights import controller, database
from insights.settings import get_environment

app = FastAPI(
    title="Insights",
    version="0.1.0",
    openapi_url="/api/openapi.json",
    docs_url="/api/docs",
    redoc_url="/api/redoc",
)
app.add_middleware(
    CORSMiddleware,
    allow_credentials=False,
    allow_methods=["*"],
    allow_origins=["*"],
    allow_headers=["*"],
)
app.include_router(controller.router)


@app.on_event("startup")
def startup() -> None:
    env = get_environment()
    database.init(env)
