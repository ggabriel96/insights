import { Component } from '@angular/core';
import { CardsService } from '../../api';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { catchError, finalize, tap, throwError } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.scss'],
})
export class CreateCardComponent {
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  loading = false;
  textInput: string = '';
  tagsInput: string[] = [];

  constructor(
    private cardsService: CardsService,
    private snackBar: MatSnackBar
  ) {}

  addTag(event: MatChipInputEvent): void {
    const value = (event.value || '').trim().toUpperCase();
    if (value && this.tagsInput.indexOf(value) === -1) {
      this.tagsInput.push(value.toUpperCase());
    }
    event.chipInput!.clear();
  }

  createCard(): void {
    this.loading = true;
    this.cardsService
      .createApiCardsPost({
        text: this.textInput,
        tags: this.tagsInput,
      })
      .pipe(
        catchError((error) => {
          this.snackBar.open('Erro inesperado ao criar Insight', 'OK', {
            duration: 0,
          });
          return throwError(() => error);
        }),
        finalize(() => (this.loading = false)),
        tap(() => {
          this.snackBar.open('Insight criado com sucesso!', 'OK');
          this.textInput = '';
          this.tagsInput = [];
        })
      )
      .subscribe();
  }

  removeTag(tag: string): void {
    const index = this.tagsInput.indexOf(tag);
    if (index >= 0) {
      this.tagsInput.splice(index, 1);
    }
  }
}
