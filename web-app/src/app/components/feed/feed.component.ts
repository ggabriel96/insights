import { Component, OnInit } from '@angular/core';
import { Card, CardsService } from '../../api';
import { catchError, finalize, tap, throwError } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit {
  readonly pageSize = 10;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  cards: Card[] = [];
  loading = false;
  pageNumber = 1;
  filterTags: string[] = [];

  constructor(
    private cardsService: CardsService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.loadCards();
  }

  addFilterTag(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.filterTags.push(value.toUpperCase());
      this.pageNumber = 1;
      this.loadCards();
    }
    event.chipInput!.clear();
  }

  loadCards(): void {
    this.loading = true;
    this.cardsService
      .searchApiCardsSearchPost({
        tags: this.filterTags.length === 0 ? undefined : this.filterTags,
        options: {
          pagination: {
            page_number: this.pageNumber,
            page_size: this.pageSize,
          },
        },
      })
      .pipe(
        catchError((error) => throwError(() => error)),
        finalize(() => (this.loading = false)),
        tap((results) => {
          if (results.length > 0) {
            if (this.pageNumber === 1) this.cards = results;
            else this.cards = this.cards.concat(results);
          } else {
            this.snackBar.open(
              'Não há mais Insights para carregar',
              undefined,
              { duration: 1500 }
            );
            this.pageNumber -= 1;
          }
        })
      )
      .subscribe();
  }

  loadMore(): void {
    this.pageNumber += 1;
    this.loadCards();
  }

  removeFilterTag(tag: string): void {
    const index = this.filterTags.indexOf(tag);
    if (index >= 0) {
      this.filterTags.splice(index, 1);
      this.pageNumber = 1;
      this.loadCards();
    }
  }
}
