import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeHeaderComponent } from './components/home-header/home-header.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './shared/material.module';
import { ApiModule, BASE_PATH } from './api';
import { HttpClientModule } from '@angular/common/http';
import { FeedComponent } from './components/feed/feed.component';
import { CardComponent } from './components/card/card.component';
import { CreateCardComponent } from './components/create-card/create-card.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeHeaderComponent,
    FeedComponent,
    CardComponent,
    CreateCardComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    HttpClientModule,
    ApiModule,
    FormsModule,
  ],
  providers: [{ provide: BASE_PATH, useValue: 'http://localhost:8080' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
